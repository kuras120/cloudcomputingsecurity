from PIL import Image
from os import listdir
from os.path import isfile, join


def get_files(source_path):
    return [filename for filename in listdir(source_path) if isfile(join(source_path, filename))]


def decode_message(source_path, results_path):
    files = get_files(source_path)
    print('Files to check: ', files)
    loops = len(files) - 1
    while loops > 0:
        dropped_path = files.pop(0)
        print(files)
        background = Image.open(join(source_path, dropped_path))
        for file_path in files:
            overlay = Image.open(join(source_path, file_path))

            background = background.convert("RGBA")
            overlay = overlay.convert("RGBA")

            new_img = Image.blend(background, overlay, 0.5)
            new_img.save(join(results_path, dropped_path + 'x' + file_path), "PNG")
        loops -= 1
