# LAB 1
Aby uruchomić zadania należy wywołać komendę python (python3 bez enva) main.py, uprzednio instalując zależności z requirements.txt
## Zadanie 1
### Przechwyciłeś 10 różnych obrazów. 2 z nich są udziałami sekretnego obrazka zawierającego hasło, reszta to przypadkowy szum. <br /> Znajdź sekretne hasło.

Zadanie zostało zrealizowane w pliku zad1.py. Składa się z metod get_files() oraz decode_message(). Pierwsza metoda zbiera w listę wszystkie dostępne pliki. <br /> Następnie druga metoda składa dwa pliki ze sobą. Takich kombinacji jest stosunkowo niewiele, więc wystarczy sprawdzić ręcznie, który plik zawiera wiadomość.

## Zadanie 2
### Utwórz dwa własne udziały w oparciu o dowolny (prosty), czarno-biały obraz. <br /> Upewnij się, że po nałożeniu ich na siebie odtworzysz sekret (pierwotny obraz). Zapisz oba udziały jako pliki .png.

Zadanie zostało zrealizowane w pliku zad2.py. Jest nieco obszerniejsze. Zawiera tuple z patternami potrzebnymi do zakodowania pixeli. <br /> Następnie logika podzielona jest na trzy elementy: 
- przygotowanie pustych obrazów do rysowania oraz konwersja inputu na obraz czarno-biały,
- rysowanie obrazu w oparciu o pattern,
- zapis obrazu do pliku

Po zakończonym algorytmie następuje wywołanie zadania pierwszego w celu weryfikacji poprawności.

## Wyniki
### Zadanie 1
Wynikowy plik znajduje się w głównym katalogu: 3.pngx10.png. Reszta wygenerowanych plików umieszczona jest w folderze results_zad1.

### Zadanie 2
Pliki wynikowe algorytmu znajdują się w results_zad2. Odszyfrowany format został umieszczony w results_zad2_reverted.

## Credentials
Bazuje na:
- https://gist.github.com/deibit/ccc2b55ae9eab94392e4118c05aded52
- https://stackoverflow.com/questions/10640114/overlay-two-same-sized-images-in-python