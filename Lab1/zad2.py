from PIL import Image, ImageDraw
from os.path import join
import random

patterns = ((1, 1, 0, 0), (1, 0, 1, 0), (0, 1, 0, 1), (0, 0, 1, 1), (1, 0, 0, 1), (0, 1, 1, 0))


def prepare_images(path):
    image = Image.open(path)
    image = image.convert('1')
    print('Image size: ', image.size)
    image_1 = Image.new('1', (image.width, image.height))
    draw_board_1 = ImageDraw.Draw(image_1)
    image_2 = Image.new('1', (image.width, image.height))
    draw_board_2 = ImageDraw.Draw(image_2)
    return image, image_1, image_2, draw_board_1, draw_board_2


def draw_image(output_draw, x, y, pattern, revert):
    if revert:
        output_draw.point((x, y), 1- pattern[0])
        output_draw.point((x + 1, y), 1 - pattern[1])
        output_draw.point((x, y + 1), 1 - pattern[2])
        output_draw.point((x + 1, y + 1), 1 - pattern[3])
    else:
        output_draw.point((x, y), pattern[0])
        output_draw.point((x + 1, y), pattern[1])
        output_draw.point((x, y + 1), pattern[2])
        output_draw.point((x + 1, y + 1), pattern[3])


def encode_image(source_path, dest_path):
    (input_image, output_image_1, output__image_2, output_draw_1, output_draw_2) = prepare_images(source_path)
    for x in range(input_image.size[0]):
        for y in range(input_image.size[1]):
            pixel = input_image.getpixel((x, y))
            pattern = random.choice(patterns)
            draw_image(output_draw_1, x, y, pattern, False)
            if pixel == 0:
                draw_image(output_draw_2, x, y, pattern, True)
            else:
                draw_image(output_draw_2, x, y, pattern, False)
    output_image_1.save(join(dest_path, 'image1.png'), 'PNG')
    output__image_2.save(join(dest_path, 'image2.png'), 'PNG')
