import zad1, zad2


if __name__ == '__main__':
    # Rozwiazaniem zadania jest obrazek 3.pngx10.png widoczny w glownym katalogu
    zad1.decode_message('data_zad1', 'results_zad1')
    # Rozwiazaniem zadania sa obrazki w katalogu results_zad2
    # W katalogu results_zad2_reverted znajduje sie obraz zlozony z dwoch wczesniej wygenerowanych
    zad2.encode_image('data_zad2/walen.png', 'results_zad2')
    zad1.decode_message('results_zad2', 'results_zad2_reverted')
